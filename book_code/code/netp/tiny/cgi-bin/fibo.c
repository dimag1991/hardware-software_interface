/*
 * adder.c - a minimal CGI program that adds two numbers together
 */
/* $begin adder */
#include "csapp.h"

int f(int n) {
	if(n == 0 || n == 1) {
		return n;
	}
	return f(n-1) + f(n-2);
}


int main(void) {
    char *buf;
    char arg[MAXLINE], content[MAXLINE];
    int n=0;

    /* Extract the two arguments */
    if ((buf = getenv("QUERY_STRING")) != NULL) {
		strcpy(arg, buf);
		n = atoi(arg);
    }

    /* Make the response body */
    sprintf(content, "Welcome to fibo.com: ");
    sprintf(content, "%sTHE Internet fibonacci portal.\r\n<p>", content);
	sprintf(content, "%sThe answer is: %d's fibonacci number = %d\r\n<p>", 
	    content, n, f(n));
    sprintf(content, "%sThanks for visiting!\r\n", content);
  
    /* Generate the HTTP response */
    printf("Content-length: %d\r\n", (int)strlen(content));
    printf("Content-type: text/html\r\n\r\n");
    printf("%s", content);
    fflush(stdout);
    exit(0);
}
/* $end adder */
