/* 
 * trans.c - Matrix transpose B = A^T
 *
 * Each transpose function must have a prototype of the form:
 * void trans(int M, int N, int A[N][M], int B[M][N]);
 *
 * A transpose function is evaluated by counting the number of misses
 * on a 1KB direct mapped cache with a block size of 32 bytes.
 */ 
#include <stdio.h>
#include "cachelab.h"

int is_transpose(int M, int N, int A[N][M], int B[M][N]);

/* 
 * transpose_submit - This is the solution transpose function that you
 *     will be graded on for Part B of the assignment. Do not change
 *     the description string "Transpose submission", as the driver
 *     searches for that string to identify the transpose function to
 *     be graded. 
 */
char transpose_submit_desc[] = "Transpose submission";
void transpose_submit(int M, int N, int A[N][M], int B[M][N])
{
    int blockSize;
    int i, j, k, l;
    //flags and helper variables to deal with accessing of diagonal elements
    int is_diag_accessed = 0, diag_index, temp;
    if(M == 32 && N == 32) {
    	blockSize = 8; //block size for good cache performance
    } else if(M == 64 && N == 64) { //COMMENTS ARE THE SAME AS IN CASE 32x32 matrix
    	blockSize = 4;
    } else if(M == 61 && N == 67) { //COMMENTS ARE ALMOST THE SAME AS IN CASE 32x32 matrix
    	blockSize = 20;
    }
    for(i = 0; i < N; i += blockSize) {
		for(j = 0; j < M; j += blockSize) {
			for(k = i; k < i + blockSize; k++) {
				for(l = j; l < j + blockSize; l++) {
				    //if out of bounds, simply continue loop execution
				    //until both indicies become in legal ranges
					if(k >= N || l >= M) {
						continue;
					}
					/* Because of diagonal elements of A and A' are the same,
				     * we don't need to access two matricies at the same operation
				     * (accessing two matricies at the same operation is a major 
				     * bottleneck that produce additional cache misses).
				     * Instead, in this case we store information about accessed diagonal element
				     * in flag and temporal variables for later use after processing
				     * current matrix' block *row*. By processing *block row*, we might
				     * meet at most one diagonal element, so, stored in flags information
				     * cannot be overwritten before it is used.
				     */
				    if(k == l) {
				        is_diag_accessed = 1;
				        diag_index = k; 
				        temp = A[k][l];
				    } else  {
						B[l][k] = A[k][l];
					}
			    }
			    //store diagonal matrix element stored in temporal variable and reset flag to 0
			    if(is_diag_accessed) {
					B[diag_index][diag_index] = temp;
					is_diag_accessed = 0;
				}
			}
		}
	}
}

/* 
 * You can define additional transpose functions below. We've defined
 * a simple one below to help you get started. 
 */ 

/* 
 * trans - A simple baseline transpose function, not optimized for the cache.
 */
char trans_desc[] = "Simple row-wise scan transpose";
void trans(int M, int N, int A[N][M], int B[M][N])
{
    int i, j, tmp;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            tmp = A[i][j];
            B[j][i] = tmp;
        }
    }    

}

/*
 * registerFunctions - This function registers your transpose
 *     functions with the driver.  At runtime, the driver will
 *     evaluate each of the registered functions and summarize their
 *     performance. This is a handy way to experiment with different
 *     transpose strategies.
 */
void registerFunctions()
{
    /* Register your solution function */
    registerTransFunction(transpose_submit, transpose_submit_desc); 

    /* Register any additional transpose functions */
    registerTransFunction(trans, trans_desc); 

}

/* 
 * is_transpose - This helper function checks if B is the transpose of
 *     A. You can check the correctness of your transpose by calling
 *     it before returning from the transpose function.
 */
int is_transpose(int M, int N, int A[N][M], int B[M][N])
{
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; ++j) {
            if (A[i][j] != B[j][i]) {
                return 0;
            }
        }
    }
    return 1;
}

