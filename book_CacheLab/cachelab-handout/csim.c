#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cachelab.h"

#define END_OF_FILE 0
#define INSTRUCTION_MEMORY_ACCESS 1
#define DATA_MEMORY_ACCESS 2

#define LOAD 'L'
#define STORE 'S'
#define MODIFY 'M'
#define INSTRUCTION 'I'

#define WORD_SIZE 64

#define ALL_ONES_BIT_PATTERN ~0x00ul

//model time ticks in LRU replacement algorithm
long unsigned int timer = 0;

//counts number of hits, misses and evictions
typedef struct {
	int hits;
	int misses;
	int evictions;
} cache_statistics;

/*
 * Even though for goals of this lab it is not nedded to store explicit block data structure,
 * I keep it here for completeness
 */
typedef struct {
	int valid; 
	unsigned long int tag; 
	unsigned long int time_of_last_use;
	char* block; 
} cache_line;

typedef struct {
	cache_line* lines;
} cache_set;

typedef struct {
	cache_set* sets;
	int s, E, b;
	cache_statistics* statistics;
	unsigned long int set_mask;
	unsigned long int tag_mask;
} cache;

cache* L1_cache;

//initialize simulated cache data structures
void init_cache(int s, int E, int b) {
	int S = 1 << s;
	int B = 1 << b;
	L1_cache = (cache*) malloc(sizeof(cache));
	L1_cache->s = s;
	L1_cache->E = E;
	L1_cache->b = b;
	L1_cache->set_mask = ((ALL_ONES_BIT_PATTERN >> b) << (WORD_SIZE - s)) >> (WORD_SIZE - s - b);
	L1_cache->tag_mask = (ALL_ONES_BIT_PATTERN >> (s + b)) << (s + b);
	L1_cache->sets = (cache_set*) malloc(sizeof(cache_set)*S);
	L1_cache->statistics = (cache_statistics*) malloc(sizeof(cache_statistics));
	L1_cache->statistics->hits = 0;
	L1_cache->statistics->misses = 0;
	L1_cache->statistics->evictions = 0;
	int i, j;
	for(i = 0; i < S; i++){
		(L1_cache->sets + i)->lines = (cache_line*) malloc(sizeof(cache_line)*E);
	}
	for(i = 0; i < S; i++){
		for(j = 0; j < E; j++){
			((L1_cache->sets + i)->lines + j)->block = (char*) malloc(B);
			((L1_cache->sets + i)->lines + j)->valid = 0;
			((L1_cache->sets + i)->lines + j)->tag = 0;
			((L1_cache->sets + i)->lines + j)->time_of_last_use = -1;
		}
	}
}

//helper function to convert between hex-character and integer number 
int get_digit(char ch) {
	if(ch >= '0' && ch <= '9') {
		return ch - '0';
	} 
	if(ch >= 'a' && ch <= 'f') {
		return (ch + 10 - 'a');
	} 
	return -1;
}

//sets kind of performed instruction and address of memory to accsess
int parse_trace_line(char* instruction, unsigned long int* address, int* size, FILE* trace_file) {
	char cursor;
    unsigned long int accum = 0;
    cursor = fgetc(trace_file);
	if(feof(trace_file)) {
		return END_OF_FILE;
	}
	if(cursor == ' ') {
		cursor = fgetc(trace_file);
	} 
	if(cursor == STORE) {
		*instruction = STORE;
	} else if(cursor == MODIFY) {
		*instruction = MODIFY;
	} else if(cursor == LOAD) {
		*instruction = LOAD;
	} else if(cursor == INSTRUCTION) {
		*instruction = INSTRUCTION;
		while(1){
			cursor = fgetc(trace_file);
			if(cursor == '\n'){
				break;
			}
		}
        return INSTRUCTION_MEMORY_ACCESS;
	}
	//skip space
	fgetc(trace_file);
	while(1) {
		cursor = fgetc(trace_file);
		if(cursor == ',') {
			break;
		}
		accum = 16*accum + get_digit(cursor);
	}
	*address = accum;
	accum = 0;
	while(1) {
		cursor = fgetc(trace_file);
		if(cursor == '\n') {
			break;
		}
		accum = 10*accum + (cursor-'0');
	}
	*size = accum;
	return DATA_MEMORY_ACCESS;
}

/*
 * check whether or not corresponding block of physical memory is in cache
 * returns 1 if yes, 0 otherwise
 * in case of hit sets time of the last use of cache block with *timer* value
 */
int cache_check(unsigned long int address, int* set_index, unsigned long int* tag) {
	int is_hit = 0;
	*set_index = (address & L1_cache->set_mask) >> L1_cache->b;                       
	*tag = (address & L1_cache->tag_mask) >> (L1_cache->b + L1_cache->s);
	int i = L1_cache->E - 1;
	while(i >= 0) {
		cache_line* line = (L1_cache->sets + *set_index)->lines + i;
		if(line->valid && line->tag == *tag) {
			(line->time_of_last_use) = timer;
			is_hit = 1;
			break;
		}
		i--;
	}
	return is_hit;
}


/* 
 * use of LRU algorithm for handling cache misses in case if cache has no
 * invalid blocks corresponding to given address.
 * sets time of last use with *timer* value in advance because of handled miss always follow
 * cache hit because of *restart* instruction to read from cache again when some of the blocks
 * have been replaced by nessesary block
 */
int LRU_miss_handler(unsigned long int address, int set_index, unsigned long int tag) {
	int is_evicted = 0, i;
	cache_set* set = L1_cache->sets + set_index;
	for(i = L1_cache->E - 1; i >= 0; i--) {
		if(!(set->lines + i)->valid){
			(set->lines + i)->valid = 1;
			(set->lines + i)->tag = tag;
			((set->lines + i)->time_of_last_use) = timer;
			return is_evicted;
		}
	}
	is_evicted = 1;
	unsigned long int curr_min = (set->lines + L1_cache->E - 1)->time_of_last_use;
	cache_line* victim_line = (set->lines + L1_cache->E - 1);
	for(i = L1_cache->E - 2; i >= 0; i--) {
		if((set->lines + i)->time_of_last_use < curr_min) {
			victim_line = (set->lines + i);
			curr_min = (set->lines + i)->time_of_last_use;
		}
	}
	victim_line->tag = tag;
	victim_line->time_of_last_use = timer;
	return is_evicted;
}


/*
 * model memory access cycle: check cache for requested data, handle cache miss if it is the case;
 * increment timer, change values of *hits*, *misses*, *evictions* accordingly;
 * prints some helpful debugging information in case of need of verbose output
 * setting in the -v command line option
 */
void access_memory(unsigned long int address, char instruction, int verbose) {
	timer++;
	int is_evicted;
	int is_hit;
	int set_index;
	unsigned long int tag;
	is_hit = cache_check(address, &set_index, &tag);
	if(instruction == LOAD || instruction == STORE) {
		if(is_hit) {
			L1_cache->statistics->hits++;
			if(verbose) printf("hit\n");
		} else {
			L1_cache->statistics->misses++;
			is_evicted = LRU_miss_handler(address, set_index, tag);
			if(verbose) printf("miss ");
			if(is_evicted) {
				L1_cache->statistics->evictions++;
				if(verbose) printf("eviction");
			}
			if(verbose) printf("\n");
		}
	} else if(instruction == MODIFY) {
		if(is_hit) {
			L1_cache->statistics->hits += 2;
			if(verbose) printf("hit hit\n");
		} else {
			L1_cache->statistics->misses++;
			L1_cache->statistics->hits++;
			if(verbose) printf("miss ");
			is_evicted = LRU_miss_handler(address, set_index, tag);
			if(is_evicted) {
				L1_cache->statistics->evictions++;
				if(verbose) printf("eviction ");
			}
			if(verbose) printf("hit\n");
		}
	}
}

int main(int argc, char** argv)
{
    int verbose = 0, argv_shift = 0;
    if(!strcmp(argv[1], "-v")) {
    	verbose = 1;
    	argv_shift = 1;
    }
    init_cache(atoi(argv[2+argv_shift]), atoi(argv[4+argv_shift]), atoi(argv[6+argv_shift]));
    FILE* trace_file = fopen(argv[8+argv_shift], "r");
    char instruction; unsigned long int address; int size;
    while(1) {
        int return_condition = parse_trace_line(&instruction, &address, &size, trace_file);
		if(return_condition == END_OF_FILE) {
			break;
		} else if(return_condition == INSTRUCTION_MEMORY_ACCESS) {
			continue;
		}
		if(verbose) {
			printf("%c ", instruction);
    		printf("%lx,", address);
    		printf("%d ", size);
    	}
		access_memory(address, instruction, verbose);
    }
    printSummary(L1_cache->statistics->hits, L1_cache->statistics->misses, L1_cache->statistics->evictions);
    fclose(trace_file);
    return 0;
}
